## missi_pad_global-user 13 TKQ1.221114.001 V14.0.5.0.TMZMIXM release-keys
- Manufacturer: xiaomi
- Platform: kona
- Codename: pipa
- Brand: Xiaomi
- Flavor: missi_pad_global-user
- Release Version: 13
- Kernel Version: 4.19.157
- Id: TKQ1.221114.001
- Incremental: V14.0.5.0.TMZMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Xiaomi/pipa_global/pipa:13/RKQ1.211001.001/V14.0.5.0.TMZMIXM:user/release-keys
- OTA version: 
- Branch: missi_pad_global-user-13-TKQ1.221114.001-V14.0.5.0.TMZMIXM-release-keys
- Repo: xiaomi_pipa_dump
